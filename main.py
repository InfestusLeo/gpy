import psycopg2 as pg
import pandas as pd
import pygsheets
from sqlalchemy import create_engine
import json
import datetime as dt

##Declaring global variables
with open('PATH TO YOUR SQL CREDS FILE', 'r') as secrets: creds = json.load(secrets)
username = creds[0]['username']
dbname = creds[0]['dbname']
pswd = creds[0]['pass']
engine = create_engine('DBTYPE+psycopg2://USERNAME:PASSWORD@HOST:PORT/DBNAME')
gc = pygsheets.authorize(service_file='PATH TO YOUR GOOGLE API KEY')


#Creating a connection to the dB
con = pg.connect("host = //YOUR HOSTNAME dbname = {0} user = {1} password = {2} port = //YOUR PORT NUMBER".format(dbname, username, pswd))
df = pd.DataFrame()

##Testing the connection and raising a flag if error
if con:
    print('Connected')
else:
    print('Error')

##Reading the SQL query and setting up the engine
with open('/Users/oyo/Desktop/database/booking_mtd.txt','r') as q: query = q.read()
df = pd.read_sql_query(query, engine)

##Logging the info for debugging -- Optional
## with open('/Users/oyo/Desktop/database/logs.txt',"a") as file:
##     log = "BKGMTD | {0} | {1}\n".format(df['id'].shape[0], dt.datetime.now().strftime("%Y-%M-%d  %H:%M:%S"))
##     file.write(log)

##Dumping the data in GSHeets
sh = gc.open('## NAME OF THE SHEET YOU NEED TO PUSH THE DATA INTO ##')
wks = sh.worksheet_by_title('## NAME OF THE TAB, YOU NEED THE DATA IN  ##')
wks.clear((1,1))
wks.set_dataframe(df,(1,1))

##Closing the connection
con.close()
